#!/usr/bin/env bash
# PGSL cluster project
# Deploy VMs from a CSV file
# Author: Stephane Paillet
# Date: 2023-02-20

# Vars
scriptpath=$(pwd)
deployuser='root'
limit='db'
vmslist=$1

function DISPLAY_USAGE {
echo "Usage : $(basename $0) [-h] CSVfile action
Quickly and ugly script :)
where:
   -h  	   : show help command
   CSVfile : file to parse
   action  : readcsv or deploy"
}

# Function to read the CSV content
function READCSV {
  cat ${vmslist} | sed 1d |
  while IFS=',' read vmname group vcpu ram disk
    do
      echo "name: ${vmname} group: ${group} vcpu: ${vcpu} memory: ${ram} GB disk size: ${disk} GB"
   done
}

# Function to launch VMs deployement ansible playbook
function SETVAULTPASS {
  # Load vault password
  read -s -p "Enter vault password:" vault_passwd
  echo ${vault_passwd} > .vault
  echo -ne "\r"
}

function CLONEROLES {
  ansible-galaxy install -f -r ${scriptpath}/roles/requirements.yml
}

# Function to launch VMs deployement ansible playbook
function VMSDEPLOY {

  cat ${vmslist} | sed 1d |
  while IFS=',' read vmname group vcpu ram disk
    do
      #echo "${vmname}" >> hosts
      ansible-playbook -e "one_vm_name='${vmname}' one_vm_labels='${group}' \
      one_vm_vcpu='${vcpu}' one_vm_memory='${ram} GB' \
      one_vm_disk_size='${disk} GB'" \
      --vault-password-file=.vault ${scriptpath}/one-vms-deploy.yml
    done
  #rm .vault
}

function CREATEINV {
  ansible-inventory -y --list --output inventories/hosts -i inventories/inventory_opennebula.yml
}

# Function to launch VMs deployement ansible playbook
function PGINSTALL {
  ansible-playbook ${scriptpath}/pgcluster-deploy.yml -i inventories/hosts -e "ansible_user='${deployuser}'" --vault-password-file=.vault --limit ${limit}
}

# Check Usage
[[ $# -ne 2 ]] && DISPLAY_USAGE && exit 1
[[ "$1" == "-h" ]] && DISPLAY_USAGE && exit 0

# Main
if [ $2 == readcsv ]; then
  READCSV
elif [ $2 == deploy ]; then
  SETVAULTPASS
  CLONEROLES
  VMSDEPLOY
  CREATEINV
  PGINSTALL
fi
